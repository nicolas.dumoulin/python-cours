
# coding: utf-8

# # Introduction à Python

# Nicolas Dumoulin [nicolas.dumoulin@irstea.fr](mailto:nicolas.dumoulin@irstea.fr)
#
# 7 Juin 2017

# ## Préambule

# Ce document est librement réutilisable selon les termes de la licence [GNU FDL](http://www.gnu.org/licenses/fdl.txt) version 1.2 et supérieures.
#
# Vous pouvez charger ce document interactif en version notebook (extension .ipynb), en démarrant `ipython notebook` (version 2.7) depuis le répertoire contenant ce document, ou bien en faisant glisser le document sur la page web du notebook après l'avoir démarré. Voir aussi la [documentation de notebook](http://ipython.org/ipython-doc/stable/interactive/htmlnotebook.html).

# ## Qu'est-ce que Python ?

# * Un langage
# * Une suite d'outils sous licence libre
# * Une bibliothèque en standard bien fournie
# * Une plate-forme pour faire :
#  - Des applications sur poste client (Qt)
#  - Des applications pour le web (Django)
#  - Des surcouches à des codes de calculs (C, C++, Fortran, …)
#  - Du bidouillage interactif

# Ressources en lignes :
#
# * Documentation officielle : http://docs.python.org/ (Notament le *Tutorial* et la *Library reference*)
# * Dive into Python : http://www.diveintopython.net/
# * IPython mini-book : http://ipython.rossant.net/
# * SciPy Cookbook : http://www.scipy.org/Cookbook

# ## Comment démarrer

# Deux modes d'utilisation :
#
# * Mode interactif (console)
# * Exécution de script
#
# Premier contact, démarrer la console interactive : `python` (version 2.x selon les systèmes)
# Pour quitter la console : [Ctrl] + [D]
#
# `ipython` est un interpréteur plus évolué qui propose de nombreuses extensions.

# On peut taper une opération arithmétique et on obtient le résultat.

# In[1]:


2 + 5 / 2


# In[2]:


4*2


# Vous pouvez taper les mêmes commandes dans un fichier texte (extension .py) et l'exécuter :
# `python monScript.py`

# ## Variables et types de données

# Le typage d'une variable est dynamique, le type est inféré lors de l'affectation.

# In[3]:


i = 1
d = 0.7
s = "bonjour"


# In[4]:


nb = 3 + 2j
type(nb)


# In[5]:


type(i)


# In[6]:


type(d)


# In[7]:


type(s)


# La variable spéciale `_` contient la dernière valeur affichée

# In[8]:


3*2


# In[9]:


#_+2


# ### Chaînes de caractère

# In[10]:


s1 = "N'importe quoi" # avec des guillemets double, cela permet d'utiliser une apostrophe dans le texte
s2 = 'N\'importe quoi' # avec des guillemets simple, l'apostrophe doit être échappée avec l'antislash \
s3 = 'Je disais "N\'importe quoi"' # avec des guillemets simple, cela permet d'utiliser des guillemets doubles dans le texte
s4 = """Je disais "N'importe quoi" """ # avec trois guillemets doubles (ou simples), pas besoin d'échapper les guillemets et apostrophes
s5 = """Les triples guillemets permettent d'écrire
plusieurs lignes"""
print(s5)


# In[11]:


i + d


# Problème avec des types non compatibles
#
#     i + s
#
# On doit alors transtyper (*caster*) les variables

# In[12]:


"Votre valeur est : "+str(d)


# In[13]:


str(i) + s


# Variables booléenes

# In[14]:


true = True
vrai = true
faux = False


# ## Opérateurs et fonctions de base

# On les trouve sous le nom de *builtin functions* dans la documentation anglophone.
#
# La fonction `print()` permet d'afficher le contenu d'une variable, et plus généralement une chaîne de caractère.

# In[15]:


print(s1)
print("Ceci est un entier : "+str(i))


# In[16]:


j=4
print("i = %d, j = %d" % (i,j))


# %d pour les entiers, %f pour les flottants et %s pour les chaînes

# La fonction `help()` permet d'obtenir la documentation d'une fonction (python 3).

# La fonction `len` permet de connaître la taille d'un tableau

# In[17]:


len("Bonjour")


# Opérateurs et fonctions arithmétiques
#
# Documentation : http://docs.python.org/2/library/stdtypes.html#numeric-types-int-float-long-complex

# In[18]:


5/2 # division entière en python 2.x mais division flottante à partir de 3.0


# In[19]:


5.0/2


# In[20]:


5.0//2


# In[21]:


int(5.0//2) # conversion de float en int


# In[22]:


5%2 # reste de la division entière


# ### Opérateurs booléens

# In[23]:


b = True
"oui" == "oui"


# In[24]:


3+1 == 4 and 5*2 == 20


# In[25]:


b == False or not 2 == 2 # utilisation du "et" logique et de la négation


# In[26]:


not b == False and 2 != 2 # la négation ne sera appliquée qu'à la première expression


# In[27]:


not (b == False and 2 != 2) # notez la différence avec les parenthèses


# ## Structures de contrôle

# Documentation : http://docs.python.org/2/tutorial/controlflow.html
#
# Attention à l'indentation !

# In[28]:


i = 30
if i<30:
    print("petit")
elif i==30:
    print ("i vaut 30")
    if i == 30:
        print("fin")
else:
    print ("grand")
if i%5 == 0:
    print("multiple de 5")


# In[29]:


if i==30: print("OK")


# La fonction `range` permet de créer des intervalles

# In[30]:


len(range(3,10))


# In[31]:


for i in range(4):
    print(str(i) + " ici, c'est la boucle")
print("ici, c'est fini")


# In[32]:


i=0
while i<5:
    print("i monte")
    i += 1 # i = i + 1
print(i)


# ## Structures de données

# ### Les listes (tableaux)

# L'indiçage des séquences  commence à 0

# In[33]:


t = [ 'un', 'deux', 'trois', 3.14 ]
t[0]


# In[34]:


m = [[1,2],[3,4]]
m


# Documentation :
# - http://docs.python.org/2/library/stdtypes.html#sequence-types-str-unicode-list-tuple-bytearray-buffer-xrange
# - http://docs.python.org/2/library/stdtypes.html#mutable-sequence-types
#
# Les chaînes de caractères sont des tableaux

# In[35]:


s = "Bonjour, comment allez-vous?"
s[0:7] # slicing


# In[36]:


s[:7] # l'indice de début est optionnel quand c'est 0


# In[37]:


s[9:16]


# In[38]:


s[-5:]


# In[39]:


t[-1]='!' # modification du dernier élément du tableau
t


# ### Petit exercice pour suivre les références d'objets

# In[40]:


s1="un"
s2="deux"
s3 = s2
s2 = "rien"
print (s3) # s3 n'est pas modifé


# In[41]:


l2 = [2,4,6]
l3 = l2
l4 = l2[:] # le slicing produit un nouveau tableau et donc un nouvel objet
l2[2]=12
print(l3) # l3 subit la modification sur l2, car les deux référencent le même objet
print(l4) # l4 n'est pas affecté, car il ne référence pas le même objet que l2


# ### Boucles sur les tableaux

# In[42]:


print("vous" in s) # test d'appartenance


# Pour faire une boucle sur les éléments du tableau :

# In[43]:


for element in t:
    print(element)


# In[44]:


for i in range(len(t)):
          print(t[i])


# Si vous voulez aussi l'indice, la fonction `zip` est pour vous.
# `zip` retourne une liste d'éléments à 2 valeurs, qui sont donc affectés dans la boucle aux variables *indice* et *element*

# In[45]:


zip(range(len(t)),t)


# Bon, OK c'est un objet "zip" sur lequel on pourra itérer. Si on veut "voir" son contenu, on peut le convertir en liste.

# In[46]:


list(zip(range(len(t)),t))


# In[47]:


for indice,element in zip(range(len(t)),t):
    print(str(indice) + " -> " + element)


# Comment fonctionne l'affectation multiple

# In[48]:


a,b = 1,2
print ("a = "+str(a))
print("b = "+str(b))
def mafonction():
    return ["plop","tagada"]
s1,s2 = mafonction() # le premier élément du tableau retourné va dans s1, le deuxième dans s2
print(s1)


# ### Les dictionnaires

# Listes d'associations clé/valeur

# In[49]:


d = {'Janvier':31,'Février':28,'Mars':31,'Avril':30,'Mai':31,'Juin':30,
'Juillet':31,'Août':31,'Septembre':30,'Octobre':31,'Novembre':30,u'Décembre':31}
print("d = %s" % d)
print("septembre %s" % d['Septembre'])
print("boucle :")
for cle,valeur in d.items():
    if valeur==31:
        print(cle)


# In[50]:


d2 =  { "Paul" : { "categorie" : "VM1", "mean": 3.4, "sd": 1}, "Pierre" : {"categorie":"CM1", "mean":2, "sd":0.4}}
print(d2["Paul"]["mean"])
d2


# Affichage disgracieux pour les caractères accentués dû à l'encodage en unicode, mieux géré avec python 3. [Plus d'information sur la gestion de l'unicode avec Python 2.7.x ici](http://docs.python.org/2/howto/unicode.html).

# In[51]:


d.pop('Septembre')
# print(d['Septembre']) # n'existe plus
print('Septembre' in d)


# ### Un peu de programmation fonctionnelle avec les « compréhensions de listes »

# Documentation : http://docs.python.org/2/tutorial/datastructures.html#list-comprehensions

# In[52]:


t = [1,4,8,5,4,7,10]


# Comment récupérer les nombres pairs ?
#
# Avec une boucle classique :

# In[53]:


resultat = []
for element in t:
    if element%2 == 0:
        resultat.append(element)
print(resultat)


# Avec les compréhensions de listes :

# In[54]:


resultat = [element*2 for element in t if element%2 == 0]
resultat


# In[55]:


[cle for cle,valeur in d.items() if valeur==30]


# Avec la fonction `filter` et une fonction anonyme *lambda*

# In[56]:


def monfiltre(element):
    return element%2 == 0
monfiltre(6)


# In[57]:


list(filter(monfiltre, t))


# In[58]:


resultat = filter(lambda element:element%2 == 0, t) # prend en argument une fonction de filtre et la liste à filtrer
print(resultat)
resultat = list(resultat) # nécessaire en python 3 car filter retourne un objet itérable de type "filter"
print(resultat)


# In[59]:


resultat = map(lambda x:str(x), t) # applique une fonction à chaque élément
list(resultat)


# In[60]:


print(str(t))
eval(str(t))


# In[61]:


t2=t
t[0]=42
print(t)
t[0]=1


# In[62]:


map(str, t) # simplification de l'exemple précédent, car la fonction str est adéquate au `map`
t[2]=8
t2= t[:]
t[2]=5
t2


# ## Écrire des fonctions

# In[63]:


def salut(nom):
    print("Salut " + nom)
salut("Nicolas")


# In[64]:


def salut2(nom, majuscule):
    message = "Salut " + nom
    if majuscule:
        message = message.upper()
    print(message)
salut2("Nicolas", True)
# salut2("Nicolas") # erreur car il faut 2 arguments


# In[65]:


def salut3(nom, majuscule=True):
    message = "Salut " + nom
    if majuscule:
        message = message.upper()
    print(message)
salut3("Nicolas")
salut3("Nicolas", False)
salut3("Nicolas", majuscule=False) # argument nommé pour plus de clarté et lever des ambiguités
salut3(majuscule=False, nom="Nicolas")


# Fonction avec retour de valeur en utilisant l'instruction `return`

# In[66]:


def selection(liste, pair=True):
    """
    Fais quelquechose.
    """
    return [element for element in liste if (element%2 == 0) == pair]
print(selection([1,4,7,12]))
print(selection([1,4,7,12], pair=False))


# In[67]:


#help(selection)


# ## Les scripts

# Pour graver dans la silice vos fabuleuses lignes de code, vous pouvez les écrire dans un fichier.
#
# Exemple avec ce fichier script.py :
#
#     a = 3
#     b = 4
#     print(a*b)
#
# Vous pouvez déclarer plusieurs fonctions dans votre scripts et du code principal (hors fonction, qui sera exécuté) :
#
#     def salut(nom):
#       print("Salut " + nom)
#
#     salut("Nicolas")
#
# Pour exécuter ce script :
#
# * dans un terminal : `python script.py`
# * dans ipython : `%run script.py`

# In[68]:


#get_ipython().magic('run script.py')
#bonjour("Robert")


# ## Les fichiers

# In[69]:


f = open('script.py', 'r')
print("Lit la ligne suivante dans le fichier :")
print(f.readline())
print("Lit le reste du fichier")
print(f.read())
f.close()


# In[70]:


sortie = open('sortie.txt', 'w')
for valeur in ['un','deux','trois']:
    sortie.write(valeur + "\n")
sortie.close()


# In[71]:


#get_ipython().run_cell_magic('bash', '', 'cat sortie.txt')


# In[72]:


with open('sortie.txt', 'r') as f:
    num = 1
    for ligne in f:
        print(str(num) + " : " + ligne)
        num += 1


# ## Les modules

# Documentation : http://docs.python.org/2/tutorial/modules.html

# Les bibliothèques additionnelles fournissent des fonctions et données supplémentaires en utilisant des modules pour éviter les conflits de nommage. Pour accéder à ces fonctions, vous devez au préalable *importer* le module correspondant.

# In[73]:


import math
print(math.pi)
from math import pi
print(pi)
from math import *
print(pi)
from math import pi as monpi
print(monpi)


# Ces modules peuvent être fournis par votre installation python, mais aussi par de simples scripts python.

# In[74]:


import script
script.bonjour("Paul")


# Le chargement du script a provoqué l'exécution du code hors-fonction (`bonjour("Nicolas")`). Nous pouvons alors modifier notre script pour que ce code d'exemple (ou par défaut) ne soit exécuté que lorsque le script est invoqué directement :
#
#     if __name__ == "__main__":
#       bonjour("Nicolas")
#

# In[75]:


import script2
from script2 import bonjour
bonjour('Nicolas')


# In[76]:


#et_ipython().run_cell_magic('bash', '', 'cat script2.py')


# Utilisation d'arguments avec %run, équivalent à exécuter le script avec la commande `python script2.py Bernard`.

# In[77]:


#get_ipython().magic('run script2.py Bernard')


# In[78]:


#help(bonjour)


# ### Modules intéressants

# * `sys`
#
# `sys.argv` contient les arguments d'un script
#
#     if __name__ == "__main__":
#       import sys
#       bonjour(sys.argv[1])
#
# * `csv` http://docs.python.org/2/library/csv.html

# In[79]:


import csv
with open('donnees.csv','r') as f:
    for ligne in csv.reader(f):
        print(map(int,ligne))


# In[80]:


import csv
f= open('donnees.csv','r')
monCsvReader = csv.reader(f)
toutesMesDonnees = []
for ligne in monCsvReader:
    donnees = list(map(int,ligne))
    toutesMesDonnees.append(donnees)
f.close()
print(toutesMesDonnees)


# * `math` http://docs.python.org/2/library/math.html

# In[81]:


import math
math.cos(math.pi)


# In[82]:


from math import *
exp(0)


# * `request` http://docs.python-requests.org/en/master/ et `json` https://docs.python.org/3.6/library/json.html
#
# Pour s'amuser avec les données en ligne

# In[83]:


import requests
r = requests.get('http://overpass-api.de/api/interpreter?data=%5Bout%3Ajson%5D%3Bnode%5B%22short%5Fname%22%3D%22ISIMA%22%5D%3Bnode%28around%3A500%29%5B%22emergency%22%3D%22defibrillator%22%5D%3Bout%20ids%3B%0A')
data = r.json()
aeds = len(data[u'elements'])
print(str(aeds) + " défibrillateurs à moins de 500 mètres d'ici")


# ## Aperçu des classes et exceptions

# Documentation : http://docs.python.org/3/tutorial/errors.html

# In[84]:


class Signet:
    def __init__(self, name, url):
        self.name = name
        self.url = url
    def updateURL(self, url):
        self.url = url
    def test(self):
        from urllib import request, error
        try:
            request.urlopen(self.url)
        except error.URLError:
            return False
        return True
docPython = Signet("doc python", "http://docs.python.org")
print(docPython.url)
print(docPython.test())
docPython.updateURL("http://")
print(docPython.url)
print(docPython.test())


# ## Exemple de graphique avec pylab

# In[85]:


import pandas as pd
#get_ipython().magic('matplotlib inline')
import matplotlib.pyplot as plt
frame=pd.read_csv('compareDistances-absolute.csv',names=("from","to","abs","rel","odo","osm","dabs","drel","dodo","dosm"))
plt.scatter(frame['abs'],frame.rel)
plt.ylabel("Erreur relative")
plt.xlabel("Erreur absolue (km)")
plt.title("Erreurs relatives des distances OSM/OdoMatrix")
plt.xlim(xmin=0)
plt.ylim(ymin=0)
