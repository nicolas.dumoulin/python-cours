mavariable = "depuis le script"

def bonjour(nom):
  """Affiche un message de bienvenue
  """
  mavariable = "Bonjour "
  print(mavariable + nom)

if __name__=="__main__":
  import sys
  print(sys.argv)
  bonjour(sys.argv[1])
  print("mavariable = "+ mavariable)
